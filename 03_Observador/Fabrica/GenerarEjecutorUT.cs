﻿using PatronObservador.Interfaces;
using PatronObservador.Observadores;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador.Fabrica
{     // Esta clase lo que hace es Agregar el Ejecutor de Pruebas Unitarias
    class GenerarEjecutorUT : IObservadoresFactory
    {
        private static EjecutorUT singleEjecutorUT;
        public ISuscriptor GetSuscriptor()
        {
            if (singleEjecutorUT == null)
                singleEjecutorUT = new EjecutorUT();
         
            return singleEjecutorUT;
        }
    }
}
