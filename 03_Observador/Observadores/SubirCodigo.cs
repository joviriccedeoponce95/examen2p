﻿using PatronObservador.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador.Observadores
{     // Esta clase muestra al subcriptor que ya puede subir su codigo
    class SubirCodigo : ISuscriptor
    {
        public void Update(Publicador publicador)
        {
            Console.WriteLine("Subir Codigo");
        }
    }
}
