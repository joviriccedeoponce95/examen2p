﻿using PatronObservador.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador.Observadores
{     //Esta clase notifica al suscriptor que existen nuevas actualizaciones
    class Notificador:ISuscriptor
    {
        public void Update(Publicador publicador)
        {
            Console.WriteLine("Existe una actualización de código: Versión 1.1");
        }
    }
}
