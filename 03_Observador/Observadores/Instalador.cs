﻿using PatronObservador.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador.Observadores
{       // Esta clase se encarga de avisar que la version ha sido exitosamente instalado
    class Instalador : ISuscriptor
    {
        public void Update(Publicador publicador)
        {
            Console.WriteLine("Se ha instalado la versión 1.1");
        }
    }
}
