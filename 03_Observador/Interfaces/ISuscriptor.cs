﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador.Interfaces
{     // Esta interfas es para publicaciones del suscriptor
    interface ISuscriptor
    {
        void Update(Publicador publicador);
    }
}
