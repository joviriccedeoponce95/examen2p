﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador.Interfaces
{   // Esta interfas  es para obtener datos del suscriptor
    interface IObservadoresFactory
    {
        ISuscriptor GetSuscriptor();
    }
}
