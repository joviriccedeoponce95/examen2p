﻿using PatronObservador.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador
{
    class Publicador
    {
        // Es una clase hija para el registro y cotrol de los suscriptores
        /// Creo una lista con los suscritores
        
           List<ISuscriptor> suscriptors = null;

        public Publicador()
        {
            suscriptors = new List<ISuscriptor>();
        }
        // Este metodo registra a los suscriptores
        public void RegistrarSuscriptor(ISuscriptor suscriptor) {
            if (!suscriptors.Contains(suscriptor))
            {
                suscriptors.Add(suscriptor);
            }
        }
        public void EliminarSuscriptor(ISuscriptor suscriptor) {
            if (suscriptors.Contains(suscriptor))
            {
                suscriptors.Remove(suscriptor);
            }
        }
        // Este metodo elimina los suscriptores
        public void EliminarSuscriptores()
        {
            for(int i = suscriptors.Count - 1; i >= 0; i--)
            {
                suscriptors.RemoveAt(i);
            }
        }
        // Este metodo Notifica al Suscriptor/es
        public void NotificarSuscriptores() {
            foreach (ISuscriptor suscriptor in suscriptors)
            {
                suscriptor.Update(this);
            }
        }
    }
}
